package halushko.servtestloader;

import com.sun.istack.internal.NotNull;
import halushko.servtestloader.exceptions.ExitExceptionError;
import halushko.servtestloader.generators.HTMLPageGenerator;
import halushko.servtestloader.utils.CommandArguments;

import java.awt.*;
import java.io.File;
import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static halushko.servtestloader.utils.CommandArguments.*;

public class CommandExecutor {
    public static String CACHE_SPIKE = "halushko";

    public static void runCommand(@NotNull Map<CommandArguments, List<String>> args) throws ExitExceptionError {
        validateCommands(args);

        if(args.keySet().contains(HELP_SHORT) || args.keySet().contains(HELP_LONG)){
            runHelp();
        } else {

            runManyClientsLoader(
                    args.get(URL).get(0),
                    Integer.valueOf(args.get(NUMBER_OF_CLIENTS).get(0)),
                    Integer.valueOf(args.get(NUMBER_OF_EXPERIMENTS).get(0)),
                    args.get(FILE_NAME).get(0)
            );
        }
    }

    private static void validateCommands(@NotNull Map<CommandArguments, List<String>> args) throws ExitExceptionError {
        if(args.keySet().contains(HELP_SHORT) || args.keySet().contains(HELP_LONG)){
            if(args.keySet().size() != 1) {
                String s = "";
                for(CommandArguments a: args.keySet()){
                    s += a.getStringValue() + "=" + args.get(a).size() + " ";
                }
                throw new ExitExceptionError("If 'HELP' key defined then command can't contain any other parameters: " + s);
            }
        }
    }

    private static void runHelp() throws ExitExceptionError {
        StringBuilder sb = new StringBuilder("Man for this program\n\n");
        sb.append("\tArguments:\n");
        sb.append("\t\t -c : int number of clients per one experiment(default is 1)\n");
        sb.append("\t\t -u : string URL to site (default is \"http://192.168.6.83/secure/Dashboard.jspa\")\n");
        sb.append("\t\t -e : int number of experiments (default is 1)\n");
        sb.append("\t\t -f : string prefix for output file (default is current date in milliseconds (long))\n");
        sb.append("\t\t -h or -help : get help\n");
        sb.append("\tExample:\n");
        sb.append("\t\tjava -jar server_test_loader -c 25 -f exp1client25\n");
        sb.append("\t\tjava -jar server_test_loader -c 25 -e 15 -f exp15client25\n");
        sb.append("\t\tjava -jar server_test_loader -c 25\n");
        sb.append("\tEnd help\n");

        System.out.println(sb.toString());
    }

    private static void runManyClientsLoader(String destinationURL, int numberOfClients, int numberOfExperiments, String fileID) throws ExitExceptionError {
        try {
            HTMLPageGenerator experiment = new HTMLPageGenerator(destinationURL);
//            HTMLPageGenerator experiment = new HTMLPageGenerator("http://www.vk.com/id5866272");
            Set<URI> URIs = new HashSet<URI>();
            for (int i = 0; i < numberOfExperiments; i++) {
                URIs.add(experiment.generateHTMPPage(CACHE_SPIKE, numberOfClients, fileID + "_" + " " + String.format("%03d", i)));
//                Thread.sleep(15000);
            }
            for(URI uri: URIs) {
//                Desktop.getDesktop().browse(uri);
            }
        } catch (Exception e) {
            throw new ExitExceptionError(e);
        }
    }
}
