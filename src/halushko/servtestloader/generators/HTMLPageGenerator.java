package halushko.servtestloader.generators;

import java.io.*;
import java.net.URI;
import java.util.Date;

/**
 * Created by Halushko on 16.01.2016.
 * <p>
 * Class generates test HTML file and then opens it in browser
 */
public class HTMLPageGenerator {
    public final static String ELEMENT_ID = "XYZ";
    public final static String SITE_PATH = "YZX";
    public final static String LAST_LINE = "ZXY";
    public final static String MAX = "XZY";
    public final static String FILE_NAME = "YYZ";
    public final static String LAST_LINE_VALUE = "<iframe id=\"XYZ\" onload=\"showElapsedTime('XYZ', 'XZY');\" src=\"YZX\" width=\"640\" height=\"800\" scrolling=\"yes\" frameborder=\"2\" ></iframe>";

    public final String destinationURL;

    public HTMLPageGenerator(String destinationURL) {
        this.destinationURL = destinationURL;
    }

    private InputStream getIS() {
        return getClass().getClassLoader().getResourceAsStream("test_server_loader.html");
    }

    private String convert(String line, int number, String prefix, String pathToSite, String elementID, int maxCount, String fileID) {
        return line.replaceAll(LAST_LINE, LAST_LINE_VALUE).
                replaceAll(ELEMENT_ID, elementID + String.valueOf(number)).
                replaceAll(SITE_PATH, pathToSite + "?dima" + prefix + "=" + String.valueOf(number)).
                replaceAll(MAX, String.valueOf(maxCount)).
                replaceAll(FILE_NAME, String.valueOf(fileID));

    }

    public URI generateHTMPPage(String elementID, int maxCount, String fileID) throws IOException {
        PrintWriter writer = null;
        String prefix = String.valueOf(new Date().getTime());
        String fileName = String.valueOf(new Date().getTime()) + ".html";
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(getIS()));
            writer = new PrintWriter(fileName, "UTF-8");
            for (String line; (line = br.readLine()) != null; ) {
                if (!line.equals(LAST_LINE)) {
                    writer.println(convert(line, 0, prefix, destinationURL, elementID, maxCount, fileID));
                } else {
                    for (int i = 0; i < maxCount; i++) {
                        writer.println(convert(line, i, prefix, destinationURL, elementID, maxCount, fileID));
                    }
                }
            }
        } finally {
            if (writer != null) writer.close();
            if (br != null) br.close();
        }
//        Desktop.getDesktop().browse();
        return new File(fileName).toURI();
    }
}
