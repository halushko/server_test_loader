package halushko.servtestloader.utils;

import java.util.HashMap;
import java.util.Map;

public enum CommandArguments {
    FILE_NAME("-f"), URL("-u"), NUMBER_OF_EXPERIMENTS("-e"), NUMBER_OF_CLIENTS("-c"), HELP_SHORT("-h"), HELP_LONG("-help");

    private static Map<String, CommandArguments> allValuesMap = new HashMap<String, CommandArguments>();
    static {
        for (CommandArguments commandArgument: values()){
            allValuesMap.put(commandArgument.getStringValue(), commandArgument);
        }
    }

    CommandArguments(String str) {
        valueString = str;
    }

    private String valueString;

    public String getStringValue(){
        return valueString;
    }
    public static CommandArguments getEnumValue(String str){
        return allValuesMap.get(str);
    }
}
