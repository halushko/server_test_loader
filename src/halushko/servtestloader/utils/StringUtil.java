package halushko.servtestloader.utils;

public class StringUtil {
    public static boolean testIntegerIsPositive(String value){
        int testInteger = value.matches("^\\d+$") ? Integer.valueOf(value) : -1;
        return testInteger > 0;
    }
    public static boolean testIntegerIsNotNegative(String value){
        return value.matches("^\\d+$");
    }
    public static boolean testIntegerIsNotPositive(String value){
        int testInteger = value.matches("^-?\\d+$") ? Integer.valueOf(value) : 1;
        return testInteger <= 0;
    }
    public static boolean testIntegerIsNegative(String value){
        return value.matches("^-\\d+$");
    }

    public static boolean testStringNotEmpty(String str){
        return str != null && str.trim().length() != 0;
    }
}
