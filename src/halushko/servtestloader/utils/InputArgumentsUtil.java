package halushko.servtestloader.utils;

import com.sun.istack.internal.NotNull;
import halushko.servtestloader.exceptions.ExitExceptionError;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static halushko.servtestloader.utils.CommandArguments.*;

public final class InputArgumentsUtil {
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy_MM_dd.HH_mm_ss");

    public static Map<CommandArguments, List<String>> parseProgramInputArgs(final String[] args) throws ExitExceptionError {
        return new HashMap<CommandArguments, List<String>>() {{
            for (int i = 0; i < args.length; i++) {
                if (args[i] != null) {
                    List<String> params = new ArrayList<String>();
                    String key = args[i];

                    if (InputArgumentsUtil.isKey(key)) {
                        while (i + 1 < args.length && !InputArgumentsUtil.isKey(args[i + 1])) {
                            params.add(args[++i]);
                        }
                    } else {
                        if(StringUtil.testIntegerIsNotPositive(args[i])) {
                            String error = "Error: Arguments can't start from not key value. Only if there is only one argument, then it has to be positive integer value\n";
                            error += "Error: Try to use -h or -help key to get more info\n";
                            throw new ExitExceptionError(error);
                        } else {
                            key = NUMBER_OF_CLIENTS.getStringValue();
                            params.add(args[i]);
                        }
                    }

                    validateKeyAndParameters(CommandArguments.getEnumValue(key), params);
                    put(CommandArguments.getEnumValue(key), params);
                }
            }
            if (!keySet().contains(CommandArguments.HELP_SHORT) && !keySet().contains(CommandArguments.HELP_LONG)) {
                addUndefinedKeys(this);
            }
        }};
    }

    private static boolean isKey(@NotNull String arg) {
        return CommandArguments.getEnumValue(arg) != null;
    }

    private static void addUndefinedKeys(HashMap<CommandArguments, List<String>> arguments) {
        if (!arguments.keySet().contains(NUMBER_OF_CLIENTS)) {
            arguments.put(NUMBER_OF_CLIENTS, new ArrayList<String>(){{add(getDefaultNumberOfClients());}});
        }
        if (!arguments.keySet().contains(NUMBER_OF_EXPERIMENTS)) {
            arguments.put(NUMBER_OF_EXPERIMENTS, new ArrayList<String>(){{add(getDefaultNumberOfExperiments());}});

        }
        if (!arguments.keySet().contains(URL)) {
            arguments.put(URL, new ArrayList<String>(){{add(getDefaultURL());}});
        }
        if (!arguments.keySet().contains(FILE_NAME)) {
            arguments.put(FILE_NAME, new ArrayList<String>(){{add(getDefaultFileName());}});
        }
    }

    private static void validateKeyAndParameters(@NotNull CommandArguments key, @NotNull List<String> params) throws ExitExceptionError {
        for (int i = 0; i < params.size(); i++) {
            if (!StringUtil.testStringNotEmpty(params.get(i))) {
                if (i == 0) {
                    throw new ExitExceptionError("First parameter for key " + key + " is empty");
                } else {
                    throw new ExitExceptionError("Parameter for key " + key + " after parameter " + params.get(i - 1) + " is empty");
                }
            }
        }

        validateCount(key, params);
        validateInteger(key, params);
    }
    private static void validateInteger(CommandArguments key, List<String> params) throws ExitExceptionError {
        switch (key) {
            case NUMBER_OF_EXPERIMENTS:
            case NUMBER_OF_CLIENTS:
                if(StringUtil.testIntegerIsNotPositive(params.get(0))){
                    throw new ExitExceptionError("Value of param for key " + key + "is invalid. It has to be positive integer number");
                }
                break;
        }
    }
    private static void validateCount(CommandArguments key, List<String> params) throws ExitExceptionError {
        switch (key) {
            case FILE_NAME:
            case URL:
            case NUMBER_OF_EXPERIMENTS:
            case NUMBER_OF_CLIENTS:
                if(params.size() != 1){
                    throw new ExitExceptionError("Number of input params for key " + key + "is invalid. It has to be '1'");
                }
                break;
            case HELP_SHORT:
            case HELP_LONG:
                if(params.size() != 0){
                    throw new ExitExceptionError("'HELP' key can't contain any parameter");
                }
                break;
        }
    }

    private static String getDefaultFileName() {
        return DATE_FORMAT.format(Calendar.getInstance().getTime());
    }
    private static String getDefaultURL() {
        return "http://192.168.6.83/secure/Dashboard.jspa";
    }
    private static String getDefaultNumberOfExperiments() {
        return "1";
    }
    private static String getDefaultNumberOfClients() {
        return "1";
    }
}
