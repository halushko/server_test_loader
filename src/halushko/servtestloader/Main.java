package halushko.servtestloader;

import halushko.servtestloader.exceptions.ExitExceptionError;
import halushko.servtestloader.utils.InputArgumentsUtil;

public class Main {

    public static void main(String[] args) {
        try {
            CommandExecutor.runCommand(InputArgumentsUtil.parseProgramInputArgs(args));
            System.out.println("Program execution completed without errors");
        } catch (ExitExceptionError e) {
            System.out.println("Program execution has been terminate because of the arisen errors:\n");
            System.out.println(e.getLocalizedMessage());
        }
    }
}
