package halushko.servtestloader.exceptions;

/**
 * Created by Halushko on 17.01.2016.
 */
public class ExitExceptionError extends Exception {
    public ExitExceptionError(Exception e){
        super(e);
    }
    public ExitExceptionError(String e){
        super(e+"\n");
    }
}
